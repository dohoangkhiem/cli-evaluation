package khiem.java.sample;

import java.util.Arrays;

import khiem.java.sample.commoncli.CommonCLI;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CommonCLITest {
    
    private String[] arguments;
    
    @BeforeClass
    public static void setup() {
        System.out.println("======================= BEGIN COMMON CLI TEST =========================");
    }

    @Test
    public void testCommonCLI1() {
        arguments = new String[] { "ant", "--verbose", "-l", "\"C:\\Program Files\\test\\test.log\"", "-n", "10"};
        System.out.println("\nArguments: " + Arrays.asList(arguments));        
        CommonCLI.main(arguments);           
    }
    
    @Test
    public void testCommonCLI2() {
        arguments = new String[] { "ant", "-a", "--almost-all", "-C", "--block-size", "--203 5ABC45", "dir1", "file1"};
        System.out.println("\nArguments: " + Arrays.asList(arguments));        
        CommonCLI.main(arguments);           
    }
    
    @Test
    public void testUnknownOption() {
        arguments = new String[] { "ant", "-a", "--almost-all", "-C", "--block-size", "--203 5ABC45", "dir1", "file1"};
        System.out.println("\nArguments: " + Arrays.asList(arguments));       
        CommonCLI.main(arguments);           
    }
    
    @Test
    public void testUsage() {
        arguments = new String[] { "ant", "-h"};
        System.out.println("\nArguments: " + Arrays.asList(arguments));      
        CommonCLI.main(arguments);           
    }
    
    @AfterClass
    public static void teardown() {
        System.out.println("\n======================= END COMMON CLI TEST ===========================\n");
    }
}
