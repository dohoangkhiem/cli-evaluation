package khiem.java.sample.commoncompress;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.utils.IOUtils;

public class Compress {

    public static void main(String[] args) throws ArchiveException, IOException {
        new Compress().extract("/home/khiem/apache-tomcat-7.0.50.tar.gz", "/home/khiem/temp1");
        /*Compress compress = new Compress();
        File tarFile = compress.extractGzip("/home/khiem/apache-tomcat-7.0.50.tar.gz", "/home/khiem/temp1");
        compress.extract(tarFile.getAbsolutePath(), "/home/khiem/temp1");*/
    }
    
    public void extract(String archive, String outputPath) throws ArchiveException, IOException {
        File archiveFile = new File(archive);
        if (!archiveFile.isFile()) {
            throw new FileNotFoundException(archive + " does not exist or is not a file");
        }
        
        ArchiveInputStream input = new ArchiveStreamFactory()
            .createArchiveInputStream(CompressorStreamFactory.GZIP, new BufferedInputStream(new FileInputStream(archiveFile)));
        
        System.out.println(input.getClass());
        
        ArchiveEntry entry = null;
        while ((entry = input.getNextEntry()) != null) {
            File output = new File(outputPath, entry.getName());
            if (entry.isDirectory()) {
                if (!output.isDirectory()) output.mkdirs();
            } else {
                if (!output.getParentFile().exists()) {
                    output.getParentFile().mkdirs();
                }
                
                OutputStream os = new FileOutputStream(output);
                IOUtils.copy(input, os);
                os.close();
            }
        }
        
        input.close();
        
    }
    
    public File extractGzip(String gzArchive, String outputPath) throws IOException {
        File gzArchiveFile = new File(gzArchive);
        FileInputStream fin = new FileInputStream(gzArchiveFile);
        BufferedInputStream in = new BufferedInputStream(fin);
        File tarFile = new File(outputPath, gzArchiveFile.getName().substring(0, gzArchiveFile.getName().lastIndexOf(".")));
        FileOutputStream out = new FileOutputStream(tarFile);
        GzipCompressorInputStream gzIn = new GzipCompressorInputStream(in);
        final byte[] buffer = new byte[8192];
        int n = 0;
        while (-1 != (n = gzIn.read(buffer))) {
            out.write(buffer, 0, n);
        }
        out.close();
        gzIn.close();
        
        return tarFile.getAbsoluteFile();
    }
}
