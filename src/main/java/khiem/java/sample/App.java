package khiem.java.sample;

import java.util.Arrays;

import org.apache.commons.cli.ParseException;

import khiem.java.sample.commoncli.CommonCLI;
import khiem.java.sample.jcommander.JCommanderDemo;

/**
 * ant [options] [target [target2 [target3] ...]]
 * Options:
 *   -h, --help                  print this message
 *   -v, --verbose               be extra verbose
 *   -l <file>, --logfile <file>        use given file for log
 *   --listener <classname>  add an instance of class as a project listener
 *   --buildfile <file>, -f <file>, --f <file>      use given buildfile
 *   -D<property>=<value>   use value for given property
 *   --find <file>           search for buildfile towards the root of the filesystem and use it
 *   -n, --number <number>   just a nice number you like
 */
public class App {
    public static void main( String[] args ) {
        String[] arguments1;
        String[] arguments2;
        
        System.out.println("======================= TEST COMMON CLI ==============================");
        
        arguments1 = new String[] { "ant", "-h", "--verbose", "-l", "\"C:\\Program Files\\test\\test.log\"", "-n", "10"};
        System.out.println("First try: " + Arrays.asList(arguments1));
        
        try {
            CommonCLI.main(arguments1);           
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        arguments2 = new String[] { "ant", "-a", "--almost-all", "-C", "--block-size", "--203 5ABC45", "dir1", "file1"};
        System.out.println("\nSecond try: " + Arrays.asList(arguments2));
        
        try {
            CommonCLI.main(arguments2);           
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        System.out.println("======================= END TEST COMMON CLI ==========================\n");
        
        System.out.println("======================= TEST JOPT SIMPLE =============================");
        try {
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("======================= END TEST JOPT SIMPLE =========================\n");
              
        System.out.println("======================== TEST JCOMMANDER =============================");
        
        arguments1 = new String[] { "-v", "ls", "--all", "-C", "--block-size", "10", "--verbose"};
        System.out.println("First try: " + Arrays.asList(arguments1));
        
        try {
            JCommanderDemo.main(arguments1);        
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        arguments2 = new String[] { "ls", "-a", "--almost-all", "-C", "-B", "--block-size", "--203 5ABC45", "-pw", "dir1", "file1"};
        System.out.println("\nSecond try: " + Arrays.asList(arguments2));
        
        try {
            JCommanderDemo.main(arguments2);        
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("======================== END TEST JCOMMANDER =========================");
    }
}
