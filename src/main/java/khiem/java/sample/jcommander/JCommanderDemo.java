package khiem.java.sample.jcommander;

import java.util.ArrayList;
import java.util.List;

import javax.activation.CommandMap;

import khiem.java.sample.Command;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

public class JCommanderDemo {
    
    public static void main(String[] args) {
        MainCommand main = new MainCommand();
        JCommander jc = new JCommander(main);
        
        PwdCommand pwd = new PwdCommand();
        jc.addCommand(pwd);
        
        ListCommand list = new ListCommand();
        jc.addCommand(list);
            
        jc.parse(args);
        
        System.out.println(jc.getParsedCommand());
        
        Command cmd = (Command) jc.getCommands().get(jc.getParsedCommand()).getObjects().get(0);
        
        cmd.initialize();
        cmd.run(args);
        cmd.finalize();
    }
    
    @Parameters(commandDescription="Main command")
    static class MainCommand {
        @Parameter(names = {"-v", "--verbose"}, description = "print verbose output")
        private boolean verbose = false;     
    }
    
    static abstract class BaseCommand {
        @Parameter(names = {"-v", "--verbose"}, description = "print verbose output")
        protected boolean verbose = false;
    }

    @Parameters(commandNames={"ls", "list"}, commandDescription = "Listing files or directories")
    static class ListCommand extends BaseCommand implements Command {

        @Parameter(description="List of files or directories")
        private List<String> files = new ArrayList<>();
        
        @Parameter(names = {"-a", "--all"}, description = "do not hide entries starting with .")
        private boolean all = false;
        
        @Parameter(names = {"-A", "--almost-all"}, description = "do not list implied . and ..")
        private boolean almostAll = false;
        
        @Parameter(names = {"-b", "--escape"}, description = "print octal escapes for nongraphic characters")
        private boolean escape;
        
        @Parameter(names = {"-B", "--ignore-backups"}, description = "do not list implied entries ending with ~", required = true)
        private boolean ignoreBackups;
        
        @Parameter(names = {"--block-size"}, description = "use SIZE-byte blocks")
        private String blockSize;
        
        @Parameter(names = "-c", description = "with -lt: sort by, and show, ctime (time of last\nmodification of file status information)\n" 
                                                + "with -l: show ctime and sort by name\notherwise: sort by ctime")
        private boolean sort;
        
        @Parameter(names = "-C", description = "list entries by columns")
        private boolean listByColumns; 
        
        @Parameter(names = {"-pw", "--password"}, description = "Automation Engine password", password = true, echoInput = false)
        private String password;
        
        @Override
        public void initialize() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void run(String[] args) {
            System.out.println("I am List command");
            System.out.println("All = " + all);
            System.out.println("Almost All = " + almostAll);
            System.out.println("Escape = " + escape);
            System.out.println("Ignore Backups = " + ignoreBackups);
            System.out.println("Block Size = " + blockSize);
            System.out.println("Sort = " + sort);
            System.out.println("List By Columns = " + listByColumns);
            System.out.println("Password = " + password);
            System.out.println("Verbose = " + verbose);                        
            System.out.println("Files = " + files.toString());
        }

        @Override
        public void finalize() {
            // TODO Auto-generated method stub
            
        }
        
    }
    
    @Parameters(commandNames = "pwd", commandDescription = "Print current working directory")
    static class PwdCommand implements Command {

        @Override
        public void initialize() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void run(String[] args) {
            System.out.println("I am PWD command");
        }
        
        @Override
        public void finalize() {
            // TODO Auto-generated method stub
            
        }
    }
}
