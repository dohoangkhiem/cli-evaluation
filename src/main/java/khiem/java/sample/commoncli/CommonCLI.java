package khiem.java.sample.commoncli;

import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommonCLI {

    public static void main(String[] args) {
        CommandLineParser parser = new BasicParser();
        if (args.length < 1) { 
            System.out.println("Error: No command specified");
            return;
        }
        String command = args[0];
        if (AntCommand.getCommand().equals(command)) {
            AntCommand cmd = new AntCommand();
            CommandLine line = null;
            try {
                line = parser.parse(cmd.getOptions(), Arrays.copyOfRange(args, 1, args.length));
            } catch (ParseException e) {
                System.err.println("Parse exception: " + e.getMessage());
                return;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            
            // set line to command fields?
            cmd.initialize(line);
            
            // invoke command run
            cmd.run();                      
        }
    }
    
    static class AntCommand {
        
        private static final String command = "ant";
        
        private Options options;
        
        public static String getCommand() {
            return command;
        }
        
        public Options getOptions() {
            return options;
        }
        
        private boolean help;
        private boolean verbose;
        private String logFile;
        private String listener;
        private String buildFile;
        private Properties properties;
        private String find;
        private int number;
        
        public AntCommand() {
            // create the Options
            options = new Options();
            options.addOption( "h", "help", false, "print this help message" );
            options.addOption( "v", "verbose", false, "be extra verbose" );
            options.addOption(OptionBuilder.withLongOpt("logfile")
                                           .withDescription( "use given file for log")
                                           .hasArg()
                                           .withArgName("file")
                                           .create("l"));
            
            options.addOption(OptionBuilder.withLongOpt("listener")
                                           .withDescription("add an instance of class as a project listener")
                                           .hasArg()
                                           .withArgName("classname")
                                           .create());
            
            options.addOption(OptionBuilder.withLongOpt("buildfile")
                                           .withDescription("use given buildfile")
                                           .withLongOpt("--file")
                                           .hasArg()
                                           .withArgName("file")
                                           .create("f"));
            
            options.addOption(OptionBuilder.withDescription("use value for given property")
                                           .withArgName("property=value")
                                           .hasArgs(2)
                                           .withValueSeparator()
                                           .create("D"));
            
            options.addOption(OptionBuilder.withLongOpt("find")
                                           .withDescription("search for buildfile towards the root of the filesystem and use it")
                                           .hasArg()
                                           .withArgName("file")
                                           .create());
            
            options.addOption(OptionBuilder.withLongOpt("number")
                                           .withDescription("just a nice number you like")
                                           .hasArg()
                                           .withArgName("number")
                                           .withType(Integer.valueOf(1))
                                           .create("n"));
        }

        /**
         * Executes commands
         */
        public void run() {
            System.out.println("Help = " + help);
            System.out.println("Verbose = " + verbose);
            System.out.println("Log file = " + logFile);
            System.out.println("Listener = " + listener);
            System.out.println("Build file = " + buildFile);
            System.out.println("Properties = " + properties.toString());
            System.out.println("Find = " + find);
            System.out.println("Number = " + number);
            
            
            if (help) {
                System.out.println("\n== Command usage: ==\n");
                usage();
            }
        }

        /**
         * Initiates class fields from parsed command line
         * @param line
         */
        public void initialize(CommandLine line) {       
            // set option arguments to fields
            help = line.hasOption("h");
            verbose = line.hasOption("v");
            logFile = line.getOptionValue("l");
            listener = line.getOptionValue("listener");
            buildFile = line.getOptionValue("f");
            properties = line.getOptionProperties("D");
            find = line.getOptionValue("find");
            if (line.hasOption("n")) number = Integer.valueOf(line.getOptionValue("n"));
        }
        
        /**
         * Prints usage
         */
        public void usage() {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("ant", options);
        }
        
    }
}
