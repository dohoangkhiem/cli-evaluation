package khiem.java.sample;

public interface Command {
    public void initialize();
    
    public void run();
    
    public void finalize();
}
